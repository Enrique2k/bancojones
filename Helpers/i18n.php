<?php
function valid($locale) {
    return in_array($locale, ['ca', 'es']);
}

$language = $_COOKIE['lang'].'.utf8';

if (isset($_COOKIE['lang']) && valid($_GET['lang'])) {
    $language = $_COOKIE['lang'].'.utf8';
}else{
    $language = 'es.utf8';
}

putenv("LC_ALL=$language");
putenv("LC_LANG=$language");
setlocale(LC_ALL, $language);

$domain = 'messages';
bindtextdomain($domain, "../locale");
textdomain($domain);