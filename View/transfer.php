<?php
    session_start();
    if(isset($_SESSION['user'])){

    }else{
        header('Location: ../View/login.php');
    }
?>

<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <title>TRANSFERENCIAS</title>
</head>

<body>

    <?php require_once('../View/header.php'); ?>
    <?php

        session_start();
        if(!isset($_SESSION['user'])){
            header('Location: ../View/login.php');
        }
    ?>

    <h1 class="text-secondary border text-center">Transferencias</h1>

    <form action="../Controller/controller.php" method="post">

        <nav class="nav">
            <a class="nav-link" href="profile.php">Profile</a>
            <a class="nav-link" href="init.php">Init</a>
            <a class="nav-link active" href="transfer.php">Transfer</a>
            <a class="nav-link" href="logout.php">Logout</a>
        </nav>
        
        <?php
            require_once('../Models/AccountModel.php');
            session_start();
            $accounts=getAccounts($_SESSION['user']);
            for ($i=0; $i<sizeof($accounts) ;$i++){?>
                <option ><?php echo $accounts[$i]["id"] ?></option>
            <?php }
        ?>

        <div class="form-group col">

            <label for="accountNumber">IBAN:</label>
            <input name="accountNumber" type="text" class="form-control" placeholder="ES6621000418401234567891">
        
        </div>

        <div class="form-group col">

            <label for="amountOfMoney">Cantidad:</label>
            <input name="amountOfMoney" type="text" class="form-control" placeholder="€€€€€€€">
        
        </div>

        <div class="form-group col">

            <label for="annotation">Anotación:</label>
            <textarea name="annotation" id="annotationTransfer" cols="50" rows="2" placeholder="Ejemplo: PAGO ALQUILER "></textarea>
        
        </div>

        <div class="form-group col">

            <input type="hidden" class="form-control" value="transfer" name="control">
        
        </div>

        <div class="form-group col">

            <input type="submit" class="form-control btn btn-primary" name="submit" value="submit">
       
        </div>

    </form>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

</body>

</html>

<?php
