<?php
require_once('../Helpers/DBManager.php');
require_once('../Models/Client.php');
use DBManager;

function selectClient(){
$manager = new DBManager();
    try{
        $sql="SELECT * FROM cliente WHERE dni=:dni";
        $stmt = $manager->getConnection()->prepare($sql);
        $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        //print_r($result);
        return  new Client($result[0]['nombre'],$result[0]['nacimiento'],$result[0]['apellido'],$result[0]['sexo'],$result[0]['email'],$result[0]['telefono'],$result[0]['dni'],$result[0]['password'],$result[0]['imagen']);
    }catch ( PDOException $e){
        echo $e->getMessage();
    }

}


function insertClient($client){
    
    $manager = new DBManager();
    try{
        $sql="INSERT INTO cliente (nombre,nacimiento, apellido, sexo, email, telefono, dni, password) VALUES (:nombre,:nacimiento,:apellido,:sexo,:email,:telefono,:dni,:password)";
        
        $password=password_hash($client->getPass(),PASSWORD_DEFAULT,['cost'=>10]);
        
        $stmt = $manager->getConnection()->prepare($sql);
        $stmt->bindParam(':nombre',$client->getName());
        $stmt->bindParam(':nacimiento',$client->getBirthdate());
        $stmt->bindParam(':apellido',$client->getSurname());
        $stmt->bindParam(':sexo',$client->isGenre());
        $stmt->bindParam(':telefono',$client->getMobilePhone());
        $stmt->bindParam(':dni',$client->getDni());
        $stmt->bindParam(':email',$client->getEmail());
        $stmt->bindParam(':password',$password);
        
        if ($stmt->execute()){
            error_log("Funciona");
        }else{
            error_log("Error...");
        }
        
    }catch (PDOException $e){
        echo $e->getMessage();
    }
    
}


function getActualUser($dni){
    $manager = new DBManager();
    try{
        $sql='SELECT * FROM cliente WHERE dni=:dni';
        $stmt=$manager->getConnection()->prepare($sql);
        $stmt->bindParam(':dni',$dni);
        $stmt->execute();
        $result=$stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }catch (PDOException $e){
        echo $e->getMessage();
    }
}

function getUserHash($dni){
    $manager = new DBManager();
    try{
        $sql='SELECT * FROM cliente WHERE dni=:dni';
        $stmt=$manager->getConnection()->prepare($sql);
        $stmt->bindParam(':dni',$dni);
        $stmt->execute();
        $result=$stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result[0]['password'];
    }catch (PDOException $e){
        echo $e->getMessage();
    }
}

function updateClient($image, $dni)
{
    error_log('ClientModel - updateClient - Init');
    $manager = new DBManager();
    try {
        $sql = "UPDATE cliente SET imagen=:img WHERE dni=:dni";
        $stmt = $manager->getConnection()->prepare($sql);
        $stmt->bindParam(':img', $image, PDO::PARAM_LOB);
        $stmt->bindParam(':dni',$dni);
        $stmt->execute();
    } catch (PDOException $e) {
        echo $e->getMessage();
    }
    error_log('ClientModel - updateClient - End');
}

function updateMobilePhone($mobilePhone, $dni){
    $manager=new DBManager();
    try{
        $sql="UPDATE cliente SET telefono=:telefono WHERE dni=:dni";
        $stmt=$manager->getConnection()->prepare($sql);
        $stmt->bindParam(':telefono',$mobilePhone);
        $stmt->bindParam(':dni',$dni);
        $stmt->execute();
    }catch (PDOException $e){
        echo $e->getMessage();
    }
}

function updateClientEmail($email, $dni){
    $manager=new DBManager();
    try{
        $sql="UPDATE cliente SET email=:email WHERE dni=:dni";
        $stmt=$manager->getConnection()->prepare($sql);
        $stmt->bindParam(':email',$email);
        $stmt->bindParam(':dni',$dni);
        $stmt->execute();
    }catch (PDOException $e){
        echo $e->getMessage();
    }
}

function updatePassClient($oldPassword, $dni){
    $password=password_hash($oldPassword,PASSWORD_DEFAULT,['cost'=>10]);
    $manager=new DBManager();
    try{
        $sql="UPDATE cliente SET password=:password WHERE dni=:dni";
        $stmt=$manager->getConnection()->prepare($sql);
        $stmt->bindParam(':password',$password);
        $stmt->bindParam(':dni',$dni);
        $stmt->execute();
    }catch (PDOException $e){
        echo $e->getMessage();
    }
}