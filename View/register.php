<?php require_once ('../Helpers/i18n.php')?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <title>REGISTRO</title>
</head>

<body>
<?php require_once ('header.php')?>

    <h1 class="text-secondary border text-center">Registro</h1>

    <form action="../Controller/controller.php" method="post" class="container pt-1">
        <div class="form-group col">
            <label for="userName">Nombre:</label>
            <input name="userName" type="text" class="form-control" value="<?php if ( isset($_POST['userName'])) echo $_POST['userName']?>">
        </div>
        <div class="form-group col">
            <label for="userSurname">Apellidos:</label>
            <input name="userSurname" type="text" class="form-control" value="<?php if ( isset($_POST['userSurname'])) echo $_POST['userSurname']?>">
        </div>
        <div class="form-group col">
            <label for="genre">Género:</label>
            <select class="custom-select" name="genre">
                <option selected value="0">Selecciona tu género</option>
                <option value="1">Hombre</option>
                <option value="2">Mujer</option>
            </select>
        </div>
        <div class="form-group col">
            <label for="birthdate">Fecha de nacimiento:</label>
            <input name="birthdate" type="date" class="form-control" value="<?php if ( isset($_POST['birthdate'])) echo $_POST['birthdate']?>">
        </div>
        <div class="form-group col">
            <label for="dni">DNI:</label>
            <input name="dni" type="text" class="form-control" value="<?php if ( isset($_POST['dni'])) echo $_POST['dni']?>">
        </div>
        <div class="form-group col">
            <label for="mobileNumber">Numero De teléfono:</label>
            <input name="mobilePhone" type="tel" class="form-control" value="<?php if ( isset($_POST['mobilePhone'])) echo $_POST['mobilePhone']?>">
        </div>
        <div class="form-group col">
            <label for="email">Email:</label>
            <input name="email" type="email" class="form-control" value="<?php if ( isset($_POST['email'])) echo $_POST['email']?>">
        </div>
        <div class="form-group col">
            <label for="pass">Contraseña:</label>
            <input name="pass" type="password" class="form-control" value="<?php if ( isset($_POST['pass'])) echo $_POST['pass']?>">
        </div>
        <div class="form-group col">
            <label for="repeatPass">Repite tu Contraseña:</label>
            <input name="repeatPass" type="password" class="form-control" value="<?php if ( isset($_POST['pass'])) echo $_POST['pass']?>">
        </div>
        <div class="form-group col">
            <input type="hidden" class="form-control" value="register" name="control">
        </div>
        <div class="form-group col">
            <input type="submit" class="form-control btn btn-primary" name="submit" value="submit">
        </div>
    </form>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
    <?php
    if (isset($_POST['message']))
        echo $_POST['message'] . '<br/>';
    ?>
</body>

</html>