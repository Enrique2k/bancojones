<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
  <title>LOGIN</title>
</head>

<body>

<?php require_once('../View/header.php'); ?>

  <h1 class="text-secondary border text-center">Login</h1>

  <form action="../Controller/controller.php" method="post" class="container pt-1">
    
    <div class="form-group col">

      <label for="dni">DNI:</label>
      <input name="dni" type="text" class="form-control">

    </div>

    <div class="form-group col">

      <label for="pass">Contraseña:</label>
      <input name="pass" type="password" class="form-control">

    </div>

    <div class="form-group col">

      <input type="hidden" class="form-control" value="login" name="control">
    
    </div>

    <div class="form-group col">

      <input type="submit" class="form-control btn btn-primary" name="submit" value="submit">
   
    </div>

  </form>

  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
  
  <?php
    if (isset($_POST['message']))
        echo $_POST['message'] . '<br/>';
  ?>

</body>

</html>