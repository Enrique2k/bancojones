<script>
    function changeLang() {
        document.getElementById('form_lang').submit();
    }
</script>


<form method='post' action='../Controller/controller.php' id='form_lang'>

    Select Language : <select name='lang' onchange='this.form.submit()'>

        <option value='es' <?php if (isset($_COOKIE['lang']) && $_COOKIE['lang'] == 'es') {
                                echo "selected";
                            } ?>>Castellano
        </option>

        <option value='ca' <?php if (isset($_COOKIE['lang']) && $_COOKIE['lang'] == 'ca') {
                                echo "selected";
                            } ?>>Català
        </option>

    </select>

    <input type="hidden" value="switch_lang" name="switch_lang">
    
</form>