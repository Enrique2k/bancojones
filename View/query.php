<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
  <title>QUERY</title>
</head>

<body>

  <?php require_once('../View/header.php'); 
  ?>

  <?php
    session_start();
    if(!isset($_SESSION['user'])){
        header('Location: ../View/login.php');
    }
  ?>

  <h1 class="text-secondary border text-center">Query</h1>

  <form action="../Controller/controller.php" method="post">

    <nav class="nav">

      <a class="nav-link" href="profile.php">Profile</a>
      <a class="nav-link" href="init.php">Init</a>
      <a class="nav-link" href="transfer.php">Transfer</a>
      <a class="nav-link" href="logout.php">Logout</a>

    </nav>

    <?php
      session_start();
      if (isset($_SESSION['salary']))
          echo '</br></br>' . $_SESSION['salary'];
    ?>

    <div class="form-group col">

      <label for="pass">Transacciones:</label>

      <ul class="list-group">
        <li class="list-group-item">Transacción</li>
        <li class="list-group-item">Transacción</li>
        <li class="list-group-item">Transacción</li>
        <li class="list-group-item">Transacción</li>
      </ul>

    </div>

    <select class="custom-select" name="filterBankingTransaction">
      <option selected>Elige tu opción</option>
      <option value="1">Todas</option>
      <option value="2">Solo Las Recibidas</option>
      <option value="3">Solo Las Enviadas</option>
    </select>

  </form>

  <?php
    session_start();
    require_once('../Models/AccountModel.php');
    if (isset($_SESSION['lista'])) {
        $movements=$_SESSION['lista'];
        echo '<table class="default" rules="all" frame="border">';
        echo '<tr>';
        echo '<th>origen</th>';
        echo '<th>destino</th>';
        echo '<th>hora</th>';
        echo '<th>cantidad</th>';
        echo '</tr>';
        for ($i=0;$i<count($movements);$i++){
            echo '<tr>';
            echo '<td>'.$movements[$i]['id_origen'].'</td>';
            echo '<td>'.$movements[$i]['id_destino'].'</td>';
            echo '<td>'.$movements[$i]['fecha'].'</td>';
            echo '<td>'.$movements[$i]['cantidad'].'</td>';
            echo '</tr>';
        }
        echo '</table>';
    }

  ?>


  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
</body>

</html>