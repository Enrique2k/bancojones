<?php


require_once('../Models/Client.php');
require_once('../Models/ClientModel.php');
require_once('../Models/Account.php');
require_once('../Models/AccountModel.php');
require_once('../Models/TransferModel.php');
require_once('../Helpers/helper.php');

//use Client;

if (isset($_POST['submit'])) {
    if ($_POST['switch_lang'] == 'switch_lang') {
        $lang = $_POST['lang'];
        setcookie('lang', $lang, time() * 60 * 60 * 24 * 38, '/', 'localhost');
        header('location' . $_SERVER['HTTP_REFERER']);
    }

    if ($_POST['control'] == 'register') {
        $_POST['message'] = validateRegister();
        if ($_POST['message'] == 'OK') {
            $client = new Client($_POST['userName'], $_POST['birthdate'], $_POST['userSurname'], $_POST['genre'], $_POST['email'], $_POST['mobilePhone'], $_POST['dni'], $_POST['pass'], "");
            insertClient($client);
            header('Location: ../View/login.php');
        } else {
            require_once('../View/register.php');
        }
    }

    if ($_POST['control'] == 'login') {
        $hash = getUserHash($_POST['dni']);

        if (password_verify($_POST['pass'], $hash)) {

            session_start();
            $_SESSION['user'] = $_POST['dni'];
            header('Location: ../View/init.php');
        } else {
            require_once('../View/login.php');
        }

    }

    if ($_POST['control'] == 'profile') {
        session_start();
        $check = getimagesize($_FILES['upload']['tmp_name']);

        $fileName = $_FILES['upload']['name'];
        $fileSize = $_FILES['upload']['size'];
        $fileType = $_FILES['upload']['type'];

        if ($check !== false) {
            $image = file_get_contents($_FILES['upload']['tmp_name']);
            error_log('Controler - Profile' . $_FILES['upload']['tmp_name']);
            updateClient($image, $_SESSION['user']);
        }
        
        if ($_POST['mobilePhone'] != "") {
            updateMobilePhone($_POST['mobilePhone'], $_SESSION['user']);
        }

        if ($_POST['email'] != "") {
            updateClientEmail($_POST['email'], $_SESSION['user']);
        }

        if ($_POST['pass'] != "" && $_POST['repeatPass'] != "" && $_POST['pass'] == $_POST['repeatPass']) {
            if (validatePassword()) {
                updatePassClient($_POST['pass'], $_SESSION['user']);
            }
        }
        $obj = selectClient();
        $_POST['image'] = $obj;
        header('Location: ../View/profile.php');
    }

    if($_POST['control']=='create'){
        session_start();
        createAccount($_SESSION['user']);
    }

    if($_POST['control']=='select_account'){
        $salary = getSalary($_POST['accounts']);
        session_start();
        $_SESSION['salary']=$salary;
        header("Location: ../View/query.php");
    }
    if($_POST['control']=='transfer') {
        if (accountExisting($_POST['accounts']) && accountExisting($_POST['destinationAccount'])) {
            transfer($_POST['accounts'], $_POST['destinationAccount'],$_POST['amount']);
            header('Location: ../View/transfer.php');
        }
    }

    if($_POST['control']=='query') {
        session_start();
        $_SESSION['salary']=getSalary($_POST['accounts']);
        $_SESSION['list']=getMovements($_POST['accounts']);
        header("Location: ../View/query.php");
    }
    if($_POST['control']=='select_account'){
        $saldo = getSalary($_POST['accounts']);
        session_start();
        $_SESSION['salary']=$saldo;
        $_SESSION['list']=getMovements($_POST['accounts']);
        header("Location: ../View/query.php");

    }


}
