<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
  <title>PERFIL</title>
</head>

<body>

  <?php require_once('../View/header.php'); 
  ?>

  <?php
    session_start();
    if(!isset($_SESSION['user'])){
        header('Location: ../View/login.php');
    }
  ?>

  <?php
  
    session_start();
    require_once('../Models/ClientModel.php');

    $user = getActualUser($_SESSION['user']);
    $_POST['userName'] = $user[0]["userName"];
    $_POST['userSurname'] = $user[0]["userSurname"];
    $_POST['mobilePhone'] = $user[0]["mobilePhone"];
    $_POST['email'] = $user[0]["email"];
    $_POST['birthdate'] = $user[0]["birthdate"];
    $_POST['genre'] = $user[0]["genre"];
    $_POST['imagen'] = $user[0]["imagen"];

    if ($_POST['genre']) {
      $_POST['genre'] = "Hombre";
    } else {
      $_POST['genre'] = "Mujer";
    }

  ?>

  <h1 class="text-secondary border text-center">Perfil</h1>

  <nav class="nav">
    <a class="nav-link active" href="profile.php">Perfil</a>
    <a class="nav-link" href="init.php">Inicio</a>
    <a class="nav-link" href="transfer.php">Transferencia</a>
    <a class="nav-link" href="logout.php">Logout</a>
  </nav>

  <form action="../Controller/controller.php" method="post" enctype="multipart/form-data">

    <div class="form-group col">

      <label for="userName">Nombre:</label>
      <input disabled name="userName" type="text" class="form-control" value="<?php echo $_POST['userName']; ?>">
    
    </div>

    <div class="form-group col">

      <label for="userSurname">Apellidos:</label>
      <input disabled name="userSurname" type="text" class="form-control" value="<?php echo $_POST['userSurname']; ?>">
    
    </div>

    <div class="form-group col">

      <label for="genre">Género:</label>

      <select class="form-control">

        <option selected value="0"> "<?php echo $_POST['genre']; ?>" 
        </option>
        
      </select>

    </div>

    <div class="form-group col">

      <label for="birthdate">Fecha de nacimiento:</label>

      <input disabled name="birthdate" type="date" class="form-control" value="<?php echo $_POST['birthdate']; ?>">
    
    </div>

    <div class="form-group col">

      <label for="mobilePhone">Numero de teléfono:</label>

      <input name="mobilePhone" type="tel" class="form-control" value="<?php echo $_POST['mobilePhone']; ?>">
    
    </div>

    <div class="form-group col">

      <label for="email">Email:</label>
      
      <input name="email" type="email" class="form-control" value="<?php echo $_POST['email']; ?>">
    
    </div>

    <div class="form-group col">

      <label for="pass">Contraseña:</label>

      <input name="pass" type="password" class="form-control" >

    </div>

    <div class="form-group col">

      <label for="repeatPass">Repite contraseña:</label>

      <input name="repeatPass" type="password" class="form-control">

    </div>

    <div class="form-group col">

      <label for="image">Imagen de perfil:</label>

      <input name="upload" type="file" class="form-control" id="upload">

    </div>

    <div class="form-group col">

      <input type="hidden" class="form-control" value="profile" name="control">

    </div>

    <?php
      if ($_POST['imagen'] != "") {
        ob_start();
        fpassthru($_POST['imagen']);
        $data = ob_get_contents();
        ob_end_clean();
        $img = "data:image/*;base64," . base64_encode($data);
        echo "<img src='" . $img . "' style='" . "height:200px;'" . "/>";
      }
    ?>

    <div class="form-group col">

      <input type="submit" class="form-control btn btn-primary" name="submit" value="submit">

    </div>
    
  </form>

  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
</body>

</html>